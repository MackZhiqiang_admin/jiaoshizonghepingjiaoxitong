package com.yxb.teaching_assessment.biz.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: LX
 * @Date: Create in 2019/8/21 16:37
 */
@Data
public class LoginSuccessVO {

    private String userId;


    private String userPhone;


    private List<String> roles;


    private String userMobile;
}
