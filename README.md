# 教师综合评教系统

#### 介绍
项目描述：评教系统和投票系统类似，所有做起来比较快。学生登陆系统后，查看自己班级的课程，对每一位课程教师评教，投完票还可以查看自己投票的情况。同时学生的账号需要管理员给，不过学生可以在线修改密码。老师登陆系统查看自己所教的班级，以及自己的得分情况。管理员登陆系统能对用户进行管理，用户为学生和老师，能对评教的问卷的管理，老师进行分配课程，班级等操作。这个项目使用的是前后端分离，前端使用ajax进行请求
项目使用工具：mysql,tomcat,idea,svn,maven,git,jdk,ajax
项目使用技术：jwt,springboot,mybatisplus,swagger,layui,js,html,
jquery
项目功能：
学生：评教，查看评教结果，登陆注册，查看班级课程，修改密码
老师：登陆，班级课程，查看得分情况，修改密码
管理员：学生管理，老师管理，用户账号管理，班级管理，课程管理
部门管理


#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)![输入图片说明](https://images.gitee.com/uploads/images/2019/1012/112907_9621b194_1682643.png "QQ截图20191012112031.png")