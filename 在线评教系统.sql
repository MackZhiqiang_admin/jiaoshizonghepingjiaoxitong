/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : tesc

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2019-10-23 20:48:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for clazz
-- ----------------------------
DROP TABLE IF EXISTS `clazz`;
CREATE TABLE `clazz` (
  `clazzid` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `clazzno` varchar(20) DEFAULT NULL COMMENT '班级编号',
  `classname` varchar(20) DEFAULT NULL COMMENT '班级名称',
  `departmentid` int(11) DEFAULT NULL COMMENT '所属部门（学院）',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`clazzid`),
  KEY `departmentid` (`departmentid`),
  CONSTRAINT `clazz_ibfk_1` FOREIGN KEY (`departmentid`) REFERENCES `department` (`departmentid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='InnoDB free: 31744 kB';

-- ----------------------------
-- Records of clazz
-- ----------------------------
INSERT INTO `clazz` VALUES ('1', '160', '1', '4', '0');
INSERT INTO `clazz` VALUES ('2', '156', '3', '5', '0');
INSERT INTO `clazz` VALUES ('3', '2020', '6', '9', '0');
INSERT INTO `clazz` VALUES ('4', 'liu', 'liu', '4', '1');
INSERT INTO `clazz` VALUES ('5', '环境', '123123', '4', '0');
INSERT INTO `clazz` VALUES ('6', '1232', '123', '4', '0');

-- ----------------------------
-- Table structure for clazz_course
-- ----------------------------
DROP TABLE IF EXISTS `clazz_course`;
CREATE TABLE `clazz_course` (
  `ccid` int(11) NOT NULL AUTO_INCREMENT,
  `clazzid` int(11) DEFAULT NULL COMMENT '班级id',
  `courseid` int(11) DEFAULT NULL COMMENT '课程id',
  `term` varchar(20) DEFAULT NULL COMMENT '学期',
  PRIMARY KEY (`ccid`),
  KEY `fkclazzid` (`clazzid`),
  KEY `fkcourseid` (`courseid`),
  CONSTRAINT `fkclazzid` FOREIGN KEY (`clazzid`) REFERENCES `clazz` (`clazzid`),
  CONSTRAINT `fkcourseid` FOREIGN KEY (`courseid`) REFERENCES `course` (`courseid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clazz_course
-- ----------------------------
INSERT INTO `clazz_course` VALUES ('1', '1', '1', '');
INSERT INTO `clazz_course` VALUES ('2', '2', '2', null);
INSERT INTO `clazz_course` VALUES ('3', '1', '2', null);

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `courseid` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程id',
  `courseno` varchar(20) DEFAULT NULL COMMENT '课程编号',
  `coursename` varchar(20) DEFAULT NULL COMMENT '课程名称',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  KEY `courseid` (`courseid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '13', '高数', '0');
INSERT INTO `course` VALUES ('2', '123', '数据库', '0');
INSERT INTO `course` VALUES ('3', '123', '大数据', '0');
INSERT INTO `course` VALUES ('4', '139', '面向对象', '0');
INSERT INTO `course` VALUES ('5', '12', '软件工程', '0');
INSERT INTO `course` VALUES ('6', '14', '面向对象', '0');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `departmentid` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id(学院)',
  `departmentname` varchar(20) DEFAULT NULL COMMENT '部门名称',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`departmentid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('3', '天使', '0');
INSERT INTO `department` VALUES ('4', '信息', '1');
INSERT INTO `department` VALUES ('5', '地狱', '0');
INSERT INTO `department` VALUES ('6', '天堂', '1');
INSERT INTO `department` VALUES ('7', '低于', '0');
INSERT INTO `department` VALUES ('8', '财务处', '0');
INSERT INTO `department` VALUES ('9', 'dasf', '0');
INSERT INTO `department` VALUES ('10', '后勤部', '0');

-- ----------------------------
-- Table structure for index
-- ----------------------------
DROP TABLE IF EXISTS `index`;
CREATE TABLE `index` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `indexname` varchar(20) DEFAULT NULL,
  `option1` varchar(20) DEFAULT NULL,
  `score1` int(4) DEFAULT NULL,
  `option2` varchar(20) DEFAULT NULL,
  `score2` int(4) DEFAULT NULL,
  `option3` varchar(20) DEFAULT NULL,
  `score3` int(4) DEFAULT NULL,
  `option4` varchar(20) DEFAULT NULL,
  `score4` int(4) DEFAULT NULL,
  `delflg` bit(1) DEFAULT b'0',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of index
-- ----------------------------
INSERT INTO `index` VALUES ('1', '你评教时的心态是', '想对老师提出建设性意见，仔细评价', '1', '不评教会有很多不方便，随便评完', '2', '学院要求评的', '4', '想评价老师的教学质量', '6', '');
INSERT INTO `index` VALUES ('2', '你担心评教结果会对期末成绩有影响吗', '会', '1', '一般', '2', '一般', '4', '不会', '6', null);
INSERT INTO `index` VALUES ('3', '你所在的年级', '大一', '1', '大二', '2', '大三', '4', '大四', '6', '\0');
INSERT INTO `index` VALUES ('5', 'sdf', 'sdfa', '123', 'sjdfk', '123', 'sjdfk', '213', 'fsdaf', '213', null);
INSERT INTO `index` VALUES ('6', 'sdf', 'sdfa', '1', 'sjdfk', '2', 'sdf', '3', 'sfda', '4', '\0');
INSERT INTO `index` VALUES ('7', 'adsdsaddddd', 'sfa', '1', 'sf', '2', 'sf', '3', 'asf', '4', null);
INSERT INTO `index` VALUES ('8', 'safsdf', 'dfdsf', '1', 'safdsf', '2', 'safdsf', '4', 'dfsa', '2', null);
INSERT INTO `index` VALUES ('9', '你对评教积极吗', '不', '1', '还行', '2', '积极', '3', '非常积极', '4', null);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menuid` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) DEFAULT NULL COMMENT '父级id',
  `menuname` varchar(20) DEFAULT NULL COMMENT '权限名称',
  `menuurl` varchar(100) DEFAULT NULL COMMENT '访问可路径',
  `delflag` int(1) DEFAULT NULL,
  PRIMARY KEY (`menuid`),
  KEY `parentid` (`parentid`),
  CONSTRAINT `parentid` FOREIGN KEY (`parentid`) REFERENCES `menu` (`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('6', '学生', '1');
INSERT INTO `role` VALUES ('7', '老师', '0');
INSERT INTO `role` VALUES ('8', '管理员', '0');
INSERT INTO `role` VALUES ('9', '学生', '0');
INSERT INTO `role` VALUES ('10', '老师', '0');
INSERT INTO `role` VALUES ('11', '管理员', '0');
INSERT INTO `role` VALUES ('12', '', '0');

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `rmid` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  `menuid` int(11) DEFAULT NULL COMMENT '权限id',
  `_is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`rmid`),
  KEY `roleid` (`roleid`),
  KEY `menuid` (`menuid`),
  CONSTRAINT `menuid` FOREIGN KEY (`menuid`) REFERENCES `menu` (`menuid`),
  CONSTRAINT `roleid` FOREIGN KEY (`roleid`) REFERENCES `role` (`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `studentid` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生id',
  `studentno` varchar(20) DEFAULT NULL COMMENT '学号',
  `studentname` varchar(20) DEFAULT NULL COMMENT '姓名',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  `clazzid` int(11) DEFAULT NULL,
  PRIMARY KEY (`studentid`),
  KEY `studentid_clazzid` (`clazzid`),
  CONSTRAINT `studentid_clazzid` FOREIGN KEY (`clazzid`) REFERENCES `clazz` (`clazzid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('2', 'sadsaf', '刘明', '1', '1');
INSERT INTO `student` VALUES ('3', 'ff', 'ff', '1', '3');
INSERT INTO `student` VALUES ('7', 'd', '刘志强', '1', '2');
INSERT INTO `student` VALUES ('8', 'fasf', '余祥波', '0', '1');
INSERT INTO `student` VALUES ('9', '1', '彭根', '1', '3');
INSERT INTO `student` VALUES ('10', '1', 'ljfdlsk', '1', '3');
INSERT INTO `student` VALUES ('11', '1', '1', '1', '1');
INSERT INTO `student` VALUES ('12', 'sadf', 'sadf', '1', '2');
INSERT INTO `student` VALUES ('13', 'sadf', 'sadf', '1', '3');
INSERT INTO `student` VALUES ('17', 'saf', '1', '1', '4');
INSERT INTO `student` VALUES ('18', 'saf', '1', '1', '1');
INSERT INTO `student` VALUES ('19', 'saf1', '11', '1', '1');
INSERT INTO `student` VALUES ('20', 'saf111', '111', '0', '2');
INSERT INTO `student` VALUES ('21', '1', '1', '1', '3');
INSERT INTO `student` VALUES ('22', '1', '1', '1', '2');
INSERT INTO `student` VALUES ('23', 'sadfa', '1', '1', '3');
INSERT INTO `student` VALUES ('24', 'dsfaf', 'asf', '1', '3');
INSERT INTO `student` VALUES ('25', 'saf', 'asf', '0', '4');
INSERT INTO `student` VALUES ('26', 'fas', 'asfd', '0', '3');
INSERT INTO `student` VALUES ('27', 'sadf', 'as', '0', '4');
INSERT INTO `student` VALUES ('28', 'asdf', 'asdf', '0', '3');
INSERT INTO `student` VALUES ('29', '123', '123', '0', '4');
INSERT INTO `student` VALUES ('30', 'sadf', 'sadf', '0', '1');
INSERT INTO `student` VALUES ('31', '24324', '234324', '0', '3');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `teacherid` int(11) NOT NULL AUTO_INCREMENT COMMENT '教师id',
  `teacherno` varchar(20) DEFAULT NULL COMMENT '教师编号',
  `teachername` varchar(30) DEFAULT NULL COMMENT '教师姓名',
  `is_delete` int(1) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`teacherid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', '23', '23', '1');
INSERT INTO `teacher` VALUES ('2', 'sdfaf', 'wrew', '0');
INSERT INTO `teacher` VALUES ('3', '2', '刘静', '1');
INSERT INTO `teacher` VALUES ('4', '3', '碰过', '0');
INSERT INTO `teacher` VALUES ('5', 'sdfhsfhj', '立即', '1');
INSERT INTO `teacher` VALUES ('6', 'asf', 'asf', '0');
INSERT INTO `teacher` VALUES ('16', 'af', 'af', '1');
INSERT INTO `teacher` VALUES ('17', 'sadf', 'sadf', '1');
INSERT INTO `teacher` VALUES ('18', '1', '1', '1');
INSERT INTO `teacher` VALUES ('19', 'sadf', '昵称', '1');
INSERT INTO `teacher` VALUES ('20', 'a', '小李', '0');
INSERT INTO `teacher` VALUES ('21', '1', '小赵', null);

-- ----------------------------
-- Table structure for teacher_clazz_course
-- ----------------------------
DROP TABLE IF EXISTS `teacher_clazz_course`;
CREATE TABLE `teacher_clazz_course` (
  `tccid` int(11) NOT NULL AUTO_INCREMENT,
  `teacherid` int(11) DEFAULT NULL COMMENT '教师id',
  `ccid` int(11) DEFAULT NULL,
  `term` varchar(20) DEFAULT NULL,
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  `clazzid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  PRIMARY KEY (`tccid`),
  KEY `teacherid` (`teacherid`),
  KEY `ccid` (`ccid`),
  KEY `teacehrcourseid` (`courseid`),
  KEY `clazzid1` (`clazzid`),
  CONSTRAINT `clazzid1` FOREIGN KEY (`clazzid`) REFERENCES `clazz` (`clazzid`),
  CONSTRAINT `teacehrcourseid` FOREIGN KEY (`courseid`) REFERENCES `course` (`courseid`),
  CONSTRAINT `teacher_clazz_course_ibfk_1` FOREIGN KEY (`teacherid`) REFERENCES `teacher` (`teacherid`),
  CONSTRAINT `teacher_clazz_course_ibfk_2` FOREIGN KEY (`ccid`) REFERENCES `clazz_course` (`ccid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='InnoDB free: 31744 kB';

-- ----------------------------
-- Records of teacher_clazz_course
-- ----------------------------
INSERT INTO `teacher_clazz_course` VALUES ('1', '1', '1', '1', '0', '2', '3');
INSERT INTO `teacher_clazz_course` VALUES ('2', '3', '3', null, '0', '1', '3');
INSERT INTO `teacher_clazz_course` VALUES ('3', '3', '3', null, '0', '4', '2');
INSERT INTO `teacher_clazz_course` VALUES ('4', '2', '2', null, '0', '3', '2');
INSERT INTO `teacher_clazz_course` VALUES ('5', '4', null, null, '0', '2', '2');
INSERT INTO `teacher_clazz_course` VALUES ('6', '5', null, null, '0', '3', '3');
INSERT INTO `teacher_clazz_course` VALUES ('7', '6', null, null, '0', '3', '2');
INSERT INTO `teacher_clazz_course` VALUES ('8', '16', null, null, '0', '2', '1');
INSERT INTO `teacher_clazz_course` VALUES ('9', '17', null, null, '0', '4', '2');
INSERT INTO `teacher_clazz_course` VALUES ('10', '18', null, null, '0', '3', '3');
INSERT INTO `teacher_clazz_course` VALUES ('13', '21', null, null, '0', null, null);
INSERT INTO `teacher_clazz_course` VALUES ('14', '21', null, null, '0', '5', null);
INSERT INTO `teacher_clazz_course` VALUES ('15', '21', null, null, '0', '5', '6');
INSERT INTO `teacher_clazz_course` VALUES ('16', '21', null, null, '0', '5', '6');
INSERT INTO `teacher_clazz_course` VALUES ('17', '3', null, null, '0', '2', '1');
INSERT INTO `teacher_clazz_course` VALUES ('18', '4', null, null, '0', '1', '3');
INSERT INTO `teacher_clazz_course` VALUES ('19', '2', null, null, '0', '2', '2');

-- ----------------------------
-- Table structure for teacher_clazz_course_index
-- ----------------------------
DROP TABLE IF EXISTS `teacher_clazz_course_index`;
CREATE TABLE `teacher_clazz_course_index` (
  `tcciid` int(11) NOT NULL AUTO_INCREMENT,
  `tccid` int(11) DEFAULT NULL,
  `teacherid` int(11) NOT NULL,
  `indexid` int(11) DEFAULT NULL,
  `score` int(4) DEFAULT NULL,
  `clazzid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `delflag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`tcciid`,`teacherid`),
  KEY `fktccid` (`tccid`),
  KEY `fkindexid` (`indexid`),
  KEY `fkuserid` (`userid`),
  KEY `teacherid` (`teacherid`),
  KEY `clazzid` (`clazzid`),
  CONSTRAINT `clazzid` FOREIGN KEY (`clazzid`) REFERENCES `clazz` (`clazzid`),
  CONSTRAINT `fkindexid` FOREIGN KEY (`indexid`) REFERENCES `index` (`indexid`),
  CONSTRAINT `fktccid` FOREIGN KEY (`tccid`) REFERENCES `teacher_clazz_course` (`tccid`),
  CONSTRAINT `fkuserid` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`),
  CONSTRAINT `teacherid` FOREIGN KEY (`teacherid`) REFERENCES `teacher` (`teacherid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher_clazz_course_index
-- ----------------------------
INSERT INTO `teacher_clazz_course_index` VALUES ('1', '2', '2', '2', '213', '4', '9', null);
INSERT INTO `teacher_clazz_course_index` VALUES ('2', '1', '1', null, '150', null, '2', null);
INSERT INTO `teacher_clazz_course_index` VALUES ('3', '2', '3', null, '136', null, '2', null);
INSERT INTO `teacher_clazz_course_index` VALUES ('4', '8', '16', null, '221', null, '2', null);
INSERT INTO `teacher_clazz_course_index` VALUES ('5', '4', '2', null, '139', null, '2', null);
INSERT INTO `teacher_clazz_course_index` VALUES ('6', '6', '5', null, '135', null, '2', null);
INSERT INTO `teacher_clazz_course_index` VALUES ('7', '5', '4', null, '137', null, '2', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  `teacherid` int(11) DEFAULT NULL COMMENT '注册信息绑定的教师信息',
  `studentid` int(11) DEFAULT NULL COMMENT '注册信息绑定的学生信息',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`userid`),
  KEY `roleid` (`roleid`),
  KEY `teacherid` (`teacherid`) USING BTREE,
  KEY `studentid` (`studentid`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`studentid`) REFERENCES `student` (`studentid`),
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`teacherid`) REFERENCES `teacher` (`teacherid`),
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`roleid`) REFERENCES `role` (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '169000321', '222', '1', null, null, '0');
INSERT INTO `user` VALUES ('2', '2', '2', null, '2', null, '0');
INSERT INTO `user` VALUES ('3', '1', '1', null, null, null, '0');
INSERT INTO `user` VALUES ('4', '324', '2324', '3', null, null, '0');
INSERT INTO `user` VALUES ('5', '169000320', '123456', null, null, null, '0');
INSERT INTO `user` VALUES ('6', '168000322', '123456', null, null, null, '0');
INSERT INTO `user` VALUES ('7', '190', 'sdf', null, '9', null, '0');
INSERT INTO `user` VALUES ('8', '32', 'sadf', null, null, null, '0');
INSERT INTO `user` VALUES ('9', '234', 'f', null, '12', null, '0');
INSERT INTO `user` VALUES ('10', 'rweqrsd', 'f', null, null, null, '0');
INSERT INTO `user` VALUES ('11', 'we', 'f', null, null, null, '0');
INSERT INTO `user` VALUES ('12', 'sdfa', 'f', null, null, null, '0');
